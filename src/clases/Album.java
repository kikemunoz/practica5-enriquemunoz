package clases;

/**
 * @author kike
 * 
 */
import java.time.LocalDate;

public class Album {
	/**
	 * Atributos
	 */
	private String nombre;
	private int numCanciones;
	private double precio;
	private LocalDate fechaLanzamiento;
	private Artista artista;

	/**
	 * Constructor
	 */
	public Album() {
		this.nombre = "";
		this.numCanciones = 0;
		this.precio = 0;
	}
	
	

	/**
	 * Setter & Getter
	 * 
	 */
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public int getNumCanciones() {
		return numCanciones;
	}

	public void setNumCanciones(int numCanciones) {
		this.numCanciones = numCanciones;
	}

	public double getPrecio() {
		return precio;
	}

	public void setPrecio(double precio) {
		this.precio = precio;
	}

	public LocalDate getFechaLanzamiento() {
		return fechaLanzamiento;
	}

	public void setFechaLanzamiento(LocalDate fechaLanzamiento) {
		this.fechaLanzamiento = fechaLanzamiento;
	}

	public Artista getArtista() {
		return artista;
	}

	public void setArtista(Artista artista) {
		this.artista = artista;
	}

	/**
	 * toString
	 */
	@Override

	public String toString() {
		return "\n" + "Nombre de �lbum: " + nombre + "\n" + "N� canciones: " + numCanciones + "\n" + "Precio: " + precio + " �" + "\n"
				+ "Fecha lanzamiento: " + fechaLanzamiento + "\n" + artista;
	}

}
