package programa;

import java.util.Scanner;
/**
 * 
 * @autor kike
 * 
 */

import clases.Discografica;

public class Programa {

	static Scanner input = new Scanner(System.in);

	public static void main(String[] args) {

		Discografica discografica = new Discografica();
		int opcion;

		do {
			System.out.println("");
			System.out.println("Practica 5 _____________________________ Enrique Mu�oz");
			System.out.println("");
			System.out.println("______________________________________________________");
			System.out.println("");
			System.out.println("            Elige unna opci�n:               ");
			System.out.println("");
			System.out.println("1.  - Dar de alta un artista");
			System.out.println("2.  - Listar artistas");
			System.out.println("3.  - Buscar un artista");
			System.out.println("4.  - Eliminar un artista");
			System.out.println("5.  - Dar de alta un alb�m");
			System.out.println("6.  - Listar albums");
			System.out.println("7.  - Buscar un album");
			System.out.println("8.  - Eliminar un album");
			System.out.println("9.  - Listar albums por el a�o de fecha de lanzamiento");
			System.out.println("10. - Asignar artista a un �lbum");
			System.out.println("11. - Listar albums por nombre de artista");
			System.out.println("12. - Ordenar artistas alfab�ticamente");
			System.out.println("13.- Salir");
			opcion = input.nextInt();

			switch (opcion) {
			case 1:
				discografica.altaArtista();
				break;
			case 2:
				discografica.listarArtistas();
				break;
			case 3:
				System.out.println(discografica.buscarArtista());
				break;
			case 4:
				discografica.eliminarArtista();
				break;
			case 5:
				discografica.altaAlbum();
				break;
			case 6:
				discografica.listarAlbums();
				break;
			case 7:
				System.out.println(discografica.buscarAlbum());
				break;
			case 8:
				discografica.eliminarAlbum();
				break;
			case 9:
				discografica.listarAlbumAnno();
				break;
			case 10:
				discografica.asignarArtista();
				break;	
			case 11:
				discografica.listarAlbumPorNombreArtista();
				break;
			case 12:
				discografica.ordenarArtistas();
				break;
			case 13:
				System.out.println("Fin del programa.");
				System.exit(0);
				break;
			default:
				System.out.println("Opci�n no v�lida, vuelva a intentarlo.");
			}

		} while (opcion != 13);

		input.close();
	}

}
