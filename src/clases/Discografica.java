package clases;

import java.time.LocalDate;
/**
 * 
 * @autor kike
 * 
 */
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Scanner;

public class Discografica {

	/**
	 * Declaracion e inicializacion de la clase Scanner
	 */
	static Scanner input = new Scanner(System.in);

	/**
	 * Declaracion de Arraylist de Artista y de Album
	 */
	private ArrayList<Artista> listaArtistas;
	private ArrayList<Album> listaAlbums;

	/**
	 * Constructor que inicializa Arraylists de Artista y de Album
	 */
	public Discografica() {
		listaAlbums = new ArrayList<Album>();
		listaArtistas = new ArrayList<Artista>();

	}

	/**
	 * Metodo que comprueba si existe alg�n artista
	 * 
	 * @param nombre del artista
	 * 
	 * @return valor verdadero o falso
	 */
	public boolean existeArtista(String nombre) {
		for (Artista artista : listaArtistas) {
			if (artista != null && artista.getNombre().equals(nombre)) {
				return true;
			}
		}
		return false;

	}

	/**
	 * Metodo para dar de alta un artista, si el nombre del artista ya existe, no lo
	 * creara.
	 * 
	 */
	public void altaArtista() {
		System.out.println("Introduce el nombre:");
		String nombre = input.nextLine();
		if (!existeArtista(nombre)) {
			Artista nuevoArtista = new Artista();
			nuevoArtista.setNombre(nombre);
			System.out.println("Introduce el pa�s:");
			String pais = input.nextLine();
			nuevoArtista.setPais(pais);
			System.out.println("Introduce la edad:");
			int edad = input.nextInt();
			nuevoArtista.setEdad(edad);
			input.nextLine();
			System.out.println("Introduce el g�nero m�sical:");
			String generoMusical = input.nextLine();
			nuevoArtista.setGeneroMusical(generoMusical);
			listaArtistas.add(nuevoArtista);
			System.out.println("\n");
			System.out.println("Artista dado de alta.");
		} else {
			System.out.println("El artista ya existe.");
		}
	}

	/**
	 * Metodo para listar artistas
	 * 
	 */
	public void listarArtistas() {
		if (listaArtistas.size() > 0) {
			for (Artista artista : listaArtistas) {
				if (artista != null) {
					System.out.println(artista);
				}
			}
		} else {
			System.out.println("No hay ning�n artista dado de alta.");
		}
	}

	/**
	 * Metodo para buscar un artista, si el no existe ningun artista por el nombre,
	 * devuelve null.
	 * 
	 * @return Objeto Artista
	 */
	public Artista buscarArtista() {
		System.out.println("Introduce el nombre del artista:");
		String nombreArtista = input.nextLine().toLowerCase();
		for (Artista artista : listaArtistas) {
			if (artista != null && artista.getNombre().equals(nombreArtista)) {
				return artista;
			}
		}
		System.out.println("No existe ning�n artista por ese nombre.");
		return null;
	}

	/**
	 * Metodo para eliminar un artista por el nombre si existe.
	 * 
	 */
	public void eliminarArtista() {
		System.out.println("Introduce en nombre del artista a eliminar:");
		String nombreArtista = input.nextLine().toLowerCase();
		if (existeArtista(nombreArtista)) {
			Iterator<Artista> iteradorArtistas = listaArtistas.iterator();
			while (iteradorArtistas.hasNext()) {
				Artista artista = iteradorArtistas.next();
				if (artista.getNombre().equals(nombreArtista)) {
					iteradorArtistas.remove();
				}
			}
			System.out.println("Artista eliminado.");
		} else {
			System.out.println("El nombre del artista no existe.");
		}
	}

	/**
	 * Metodo que comprueba si existe alg�n album por nombre.
	 * 
	 * @param nombre del album
	 * 
	 * @return valor verdadero o falso
	 */
	public boolean existeAlbum(String nombre) {
		for (Album album : listaAlbums) {
			if (album != null && album.getNombre().equals(nombre)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Metodo para dar de alta un album, si el nombre del album ya existe, no lo
	 * creara.
	 */
	public void altaAlbum() {
		System.out.println("Introduce el nombre del �lbum:");
		String nombreAlbum = input.nextLine();
		if (!existeAlbum(nombreAlbum)) {
			Album nuevoAlbum = new Album();
			nuevoAlbum.setNombre(nombreAlbum);
			System.out.println("Introduce el n�mero de canciones:");
			int numCanciones = input.nextInt();
			nuevoAlbum.setNumCanciones(numCanciones);
			System.out.println("Introduce el precio:");
			double precio = input.nextDouble();
			input.nextLine();
			nuevoAlbum.setPrecio(precio);
			nuevoAlbum.setFechaLanzamiento(LocalDate.now());
			listaAlbums.add(nuevoAlbum);
			System.out.println("\n");
			System.out.println("Alb�m dado de alta.");

		} else {
			System.out.println("El �lbum ya existe.");
		}
	}

	/**
	 * Metodo para listar albums
	 * 
	 */
	public void listarAlbums() {
		if (listaAlbums.size() > 0) {
			for (Album album : listaAlbums) {
				if (album != null) {
					System.out.println(album);
				}
			}
		} else {
			System.out.println("No hay ning�n �lbum registrado.");
		}
	}

	/**
	 * Metodo para buscar un album, si el no existe ningun album por el nombre,
	 * devuelve null.
	 * 
	 * @return Objeto Album
	 */
	public Album buscarAlbum() {
		System.out.println("Introduce el nombre del �lbum:");
		String nombreAlbum = input.nextLine().toLowerCase();
		for (Album album : listaAlbums) {
			if (album != null && album.getNombre().equals(nombreAlbum)) {
				return album;
			}
		}
		System.out.println("No existe ning�n �lbum por ese nombre.");
		return null;
	}

	/**
	 * Metodo para eliminar un album por el nombre si existe.
	 * 
	 */
	public void eliminarAlbum() {
		System.out.println("Introduce en nombre del �lbum a eliminar:");
		String nombreAlbum = input.nextLine().toLowerCase();
		if (existeAlbum(nombreAlbum)) {
			Iterator<Album> iteradorAlbums = listaAlbums.iterator();
			while (iteradorAlbums.hasNext()) {
				Album album = iteradorAlbums.next();
				if (album.getNombre().equals(nombreAlbum)) {
					iteradorAlbums.remove();
				}
			}
			System.out.println("Alb�m eliminado.");
		} else {
			System.out.println("El nombre del �lbum no existe.");
		}
	}

	/**
	 * Metodo para listar albums por a�o
	 * 
	 */
	public void listarAlbumAnno() {
		System.out.println("Introduce el a�o a buscar:");
		int anno = input.nextInt();
		input.nextLine();
		if (listaAlbums.size() > 0) {
			for (Album albums : listaAlbums) {
				if (albums.getFechaLanzamiento().getYear() == anno) {
					System.out.println(albums);
				}
			}
		} else {
			System.out.println("No hay ning�n �lbum con fecha de lanzamiento en ese a�o");
		}
	}

	/**
	 * Metodo para asignar un artista a un album
	 * 
	 */
	public void asignarArtista() {
		Artista artista = buscarArtista();
		if (artista != null) {
			Album album = buscarAlbum();
			if (album != null) {
				album.setArtista(artista);
				System.out.println("");
				System.out.println("Artista asignado");
			}
		}
	}

	/**
	 * Metodo para listar albums por nombre de artista
	 * 
	 */
	public void listarAlbumPorNombreArtista() {
		Artista artista = buscarArtista();
		if (artista != null) {
			for (Album album : listaAlbums) {
				if (album.getArtista().equals(artista)) {
					System.out.println(album);
				} else {
					System.out.println("El artista no tiene asignado ning�n �lbum");
				}
			}
		}
	}

	/**
	 * Metodo para ordenar artistas
	 */
	public void ordenarArtistas() {
		for (int i = 0; i < listaArtistas.size() - 1; i++) {
			for (int j = i + 1; j < listaArtistas.size(); j++) {
				if (listaArtistas.get(i).getNombre().compareTo(listaArtistas.get(j).getNombre()) > 0) {
					Artista temp = listaArtistas.get(i);
					listaArtistas.set(i, listaArtistas.get(j));
					listaArtistas.set(j, temp);
				}
			}
		}
		System.out.println("Lista ordenada");
	}
}
