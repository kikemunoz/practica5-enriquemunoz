package clases;

/**
 * 
 * @author kike
 *
 */
public class Artista {
	/**
	 * Atributos
	 */
	private String nombre;
	private String pais;
	private int edad;
	private String generoMusical;

	/**
	 * Constructor
	 * 
	 */
	public Artista() {
		this.nombre = "";
		this.pais = "";
		this.edad = 0;
		this.generoMusical = "";
	}
	/**
	 * Constructor
	 * @param nombreArtista 
	 */
	

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getPais() {
		return pais;
	}

	public void setPais(String pais) {
		this.pais = pais;
	}

	public int getEdad() {
		return edad;
	}

	public void setEdad(int edad) {
		this.edad = edad;
	}

	public String getGeneroMusical() {
		return generoMusical;
	}

	public void setGeneroMusical(String generoMusical) {
		this.generoMusical = generoMusical;
	}

	/**
	 * toString
	 */
	@Override
	public String toString() {
		return "Artista: " + nombre + "\n" + "Pa�s: " + pais + "\n" + "Edad: " + edad + "\n" + "G�nero musical: "
				+ generoMusical + "\n";
	}

}
